/* Javascript for MyXBlock. */
function MyXBlock(runtime, element) {

    function updateCount(result) {
        $('.count', element).text(result.count);
    }

    var handlerUrl = runtime.handlerUrl(element, 'increment_count');

    $('p', element).click(function (eventObject) {
        $.ajax({
            type: "POST",
            url: handlerUrl,
            data: JSON.stringify({"hello": "world"}),
            success: updateCount
        });
    });

}

const form = document.getElementById('form');

function retrieveFormValue(event) {
    event.preventDefault();
    const title_block = form.querySelector('[name="title_block"]'),
        message = form.querySelector('[name="message"]');

    const values = {
        title_block: title_block.value,
        message: message.value,
    };
    console.log('myxblock', values);
}

form.addEventListener('submit', retrieveFormValue);

